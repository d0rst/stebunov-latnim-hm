package com.task.hw_7;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;


import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class WebDriverAvitoSteps {
    private WebDriver driver;
    private WebDriverWait webDriverWait;

    public WebDriverAvitoSteps() {
        System.setProperty("webdriver.chrome.driver", ConfProperties.getProperty("chromedriver"));
        this.driver = new ChromeDriver();
        this.webDriverWait = new WebDriverWait(driver, 2);
    }

    @Step("show home page")
    public void getUrl(){
        driver.get("https://www.avito.ru/");
        driver.manage().window().maximize();
    }

    @Step("select a category {category}")
    public void selectCategory(String category){
        new Select(driver.findElement(By.xpath("//select[@name=\"category_id\"]")))
                .selectByVisibleText(category);
    }

    @Step("search {search} bar input")
    public void inputSearch(String search){
        driver.findElement(By.xpath("//input[@id=\"search\"]")).sendKeys(search);
    }

    @Step("menu selection city search")
    public void selectMenuTown(){
        driver.findElement(By.xpath("//div[@class=\"main-text-2PaZG\"]")).click();
    }

    @Step("input {town} in the town search menu")
    public void searchTownAndClick(String town){
        driver.findElement(By.xpath("//div[@data-marker=\"popup-location/region\"]//input"))
                .sendKeys(town);
        webDriverWait.until(presenceOfElementLocated(
                By.xpath("//ul[@class=\"suggest-suggests-bMAdj\"]//li[@data-marker=\"suggest(0)\"]" +
                        "//*[contains(text(),'"+ town +"')]"))).click();;
        driver.findElement(By.xpath("//button[@data-marker=\"popup-location/save-button\"]"))
                .click();
    }

    @Step("checkbox check")
    public void clickCheckboxAndSearch(){
        WebElement checkbox = driver.findElement(
                By.xpath("//label[@data-marker=\"delivery-filter\"]"));
        if(checkbox.isEnabled()){
            checkbox.click();
            driver.findElement(By.xpath("//button[@data-marker=\"search-filters/submit-button\"]")).click();
        }
    }

    @Step("selection of product sorting {filter}")
    public void selectPriceFilter(String filter){
        new Select(driver.findElement(By.xpath("//div[@class=\"" +
                "sort-select-3QxXG select-select-box-3LBfK select-size-s-2gvAy\"]//select")))
                .selectByVisibleText(filter);
    }

    @Step("Поиск имени и цены первых {n} товаров ")
    public void findObjectNameAndPrice(int n){
        List<WebElement> webElements = driver.findElements(
                By.xpath("//div[@data-marker=\"catalog-serp\"]/div[@data-marker=\"item\"]"));
        for (int i = 0; i < n; i++) {
            System.out.println(webElements.get(i).findElement(By.xpath(".//h3[@itemprop=\"name\"]"))
                    .getText());
            System.out.println(webElements.get(i).findElement(By.xpath(".//meta[@itemprop=\"price\"]"))
                    .getAttribute("content") + " рублей");
        }
    }

    @Attachment
    @Step("Make screen shot of results page")
    public byte[] makeScreenShot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    @Attachment
    @Step("Make screen shot of element on the page")
    public byte[] makeAshotScreenShot(WebElement element) {
        Screenshot screenshot = new AShot().shootingStrategy(
                ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver, element);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        try {
            ImageIO.write(screenshot.getImage(), "png", buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer.toByteArray();
    }

    @Step("close the page")
    public void quit() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }
}
