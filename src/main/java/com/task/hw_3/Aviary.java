package com.task.hw_3;

import com.task.hw_3.model.animals.Animal;

import java.util.HashMap;

public class Aviary<V extends Animal>{

    private final float area;
    private final HashMap<String, V> animals;

    public Aviary(float area) {
        this.area = area;
        animals = new HashMap<>();
    }

    public boolean addAnimal(V v){
        if (v.getAviarySize().getArea() <= area) {
            animals.put(v.getName(), v);
            return true;
        }
        return false;
    }

    public void delAnimal(String k){
        animals.remove(k);
    }

    public Animal getAnimal(String k){
        return animals.get(k);
    }

}
