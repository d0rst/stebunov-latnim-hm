package com.task.hw_3;

public class WrongFoodException extends Exception {

    public WrongFoodException(){}

    public WrongFoodException(String message) {
        super(message);
    }
    
}
