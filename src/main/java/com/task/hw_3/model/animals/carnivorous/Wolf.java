package com.task.hw_3.model.animals.carnivorous;

import com.task.hw_3.model.animals.impl.Run;
import com.task.hw_3.model.animals.impl.Voice;
import com.task.hw_3.model.animals.Carnivorous;

public class Wolf extends Carnivorous implements Run, Voice {

    @Override
    public void run() {
        System.out.println("wolf run");
    }

    @Override
    public String voice() {
        return "Wolf howls";
    }

}
