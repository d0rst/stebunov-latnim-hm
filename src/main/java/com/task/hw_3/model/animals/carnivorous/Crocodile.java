package com.task.hw_3.model.animals.carnivorous;

import com.task.hw_3.model.animals.impl.Run;
import com.task.hw_3.model.animals.impl.Swim;
import com.task.hw_3.model.animals.impl.Voice;
import com.task.hw_3.model.animals.Carnivorous;

public class Crocodile extends Carnivorous implements Swim, Run, Voice {

    @Override
    public void swim() {
        this.satietyValue -= 5;
        System.out.println("crocodile swim");
    }

    @Override
    public void run() {
        this.satietyValue -= 4;
        System.out.println("crocodile run");
    }

    @Override
    public String voice() {
        this.satietyValue -= 3;
        return "Crocodile snort!";
    }
}

