package com.task.hw_3.model.animals.herbivore;

import com.task.hw_3.model.animals.impl.Voice;
import com.task.hw_3.model.animals.Herbivore;

public class Cow extends Herbivore implements Voice {

    @Override
    public String voice() {
        return "Cow hums";
    }

}
