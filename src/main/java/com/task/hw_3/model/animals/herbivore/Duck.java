package com.task.hw_3.model.animals.herbivore;

import com.task.hw_3.model.animals.Herbivore;
import com.task.hw_3.model.animals.impl.Fly;
import com.task.hw_3.model.animals.impl.Run;
import com.task.hw_3.model.animals.impl.Swim;
import com.task.hw_3.model.animals.impl.Voice;

public class Duck extends Herbivore implements Fly, Swim, Run, Voice {

    @Override
    public void fly() {
        this.satietyValue -= 5;
        System.out.println("Duck fly");
    }

    @Override
    public void run() {
        this.satietyValue -= 5;
        System.out.println("Duck run");
    }

    @Override
    public void swim() {
        this.satietyValue -= 5;
        System.out.println("Duck swim");
    }

    @Override
    public String voice() {
        return "Duck quacks";
    }

}
