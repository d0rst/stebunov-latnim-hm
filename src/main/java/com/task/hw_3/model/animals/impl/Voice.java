package com.task.hw_3.model.animals.impl;

public interface Voice {
    String voice();
}
