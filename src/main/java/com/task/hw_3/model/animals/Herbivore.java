package com.task.hw_3.model.animals;

import com.task.hw_3.WrongFoodException;
import com.task.hw_3.model.food.Food;
import com.task.hw_3.model.food.Grass;

public abstract class Herbivore extends Animal {

    @Override
    public void eat(Food food){
        super.eat(food);
        try {
            if(!(food instanceof Grass)) {
                throw new WrongFoodException();
            }
        }
        catch(WrongFoodException ex) {
            System.out.println("herbivores don't eat it");
        }
    }
}
