package com.task.hw_3.model.animals;

import com.task.hw_3.AviarySize;
import com.task.hw_3.model.food.Food;

import java.util.Objects;

public abstract class Animal {

    private String name;
    protected int satietyValue;

    private AviarySize aviarySize = AviarySize.MEDIUM;

    public void eat(Food food){
        satietyValue += food.getSatietyValue();
        System.out.println("Animal start eat food: " + food.toString());
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAviarySize(AviarySize aviarySize) {
        this.aviarySize = aviarySize;
    }

    public AviarySize getAviarySize() {
        return aviarySize;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        return o instanceof Animal &&
                ((Animal) o).name.equals(name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
