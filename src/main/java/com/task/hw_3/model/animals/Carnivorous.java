package com.task.hw_3.model.animals;

import com.task.hw_3.WrongFoodException;
import com.task.hw_3.model.food.Food;
import com.task.hw_3.model.food.Meat;

public abstract class Carnivorous extends Animal {

    @Override
    public void eat(Food food){
        super.eat(food);
        try {
            if(!(food instanceof Meat)) {
                throw new WrongFoodException();
            }
        }
        catch(WrongFoodException ex) {
            System.out.println("carnivorous don't eat it");
        }
    }

}
