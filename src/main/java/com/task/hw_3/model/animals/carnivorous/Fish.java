package com.task.hw_3.model.animals.carnivorous;

import com.task.hw_3.model.animals.impl.Swim;
import com.task.hw_3.model.animals.Carnivorous;

public class Fish extends Carnivorous implements Swim {

    @Override
    public void swim() {
        System.out.println("fish swim");
    }

}
