package com.task.hw_3.model.food;

import java.util.Date;

public abstract class Food {
    protected int satietyValue;
    protected Date shelfLife;

    public int getSatietyValue() {
        return satietyValue;
    }

    public abstract void setShelfLife(Date date);

}
