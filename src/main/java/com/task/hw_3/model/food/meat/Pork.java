package com.task.hw_3.model.food.meat;

import com.task.hw_3.model.food.Meat;

import java.util.Date;

public class Pork extends Meat {

    public Pork() {
        this.shelfLife = new Date();
        this.satietyValue = 7;
    }

    @Override
    public String toString() {
        return "Pork";
    }

}
