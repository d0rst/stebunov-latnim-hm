package com.task.hw_3.model.food.meat;

import com.task.hw_3.model.food.Meat;

import java.util.Date;

public class Mutton extends Meat {

    public Mutton() {
        this.shelfLife = new Date();
        this.satietyValue = 9;
    }

    @Override
    public String toString() {
        return "Mutton";
    }

}
