package com.task.hw_3.model.food.grass;

import com.task.hw_3.model.food.Grass;

import java.util.Date;

public class Leaves extends Grass {

    public Leaves() {
        this.shelfLife = new Date();
        this.satietyValue = 4;
    }

    @Override
    public String toString() {
        return "Leaves";
    }

}
