package com.task.hw_3.model.food.grass;

import com.task.hw_3.model.food.Grass;

import java.util.Date;

public class Vegetables extends Grass {

    public Vegetables() {
        this.shelfLife = new Date();
        this.satietyValue = 1;
    }

    @Override
    public String toString() {
        return "Vegetables";
    }

}
