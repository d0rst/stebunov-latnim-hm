package com.task.hw_3.model.food;

import java.util.Date;

public abstract class Grass extends Food {

    @Override
    public void setShelfLife(Date date) {
        this.shelfLife = date;
    }

}
