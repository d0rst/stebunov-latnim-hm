package com.task.hw_3;

public enum AviarySize{
    SMALL(1),
    MEDIUM(2),
    LARGE(3),
    VERY_LARGE(322);

    private final float area;

    AviarySize(float area){
        this.area = area;
    }

    public float getArea() {
        return area;
    }
}
