package com.task.hw_3;


import com.task.hw_3.model.animals.Animal;
import com.task.hw_3.model.animals.Carnivorous;
import com.task.hw_3.model.animals.Herbivore;
import com.task.hw_3.model.animals.carnivorous.Crocodile;
import com.task.hw_3.model.animals.carnivorous.Fish;
import com.task.hw_3.model.animals.carnivorous.Wolf;
import com.task.hw_3.model.animals.herbivore.Cow;
import com.task.hw_3.model.animals.herbivore.Duck;
import com.task.hw_3.model.animals.herbivore.Horse;
import com.task.hw_3.model.animals.impl.Swim;
import com.task.hw_3.model.animals.impl.Voice;
import com.task.hw_3.model.food.Food;
import com.task.hw_3.model.food.grass.Leaves;
import com.task.hw_3.model.food.grass.Roots;
import com.task.hw_3.model.food.grass.Vegetables;
import com.task.hw_3.model.food.meat.Beef;
import com.task.hw_3.model.food.meat.Mutton;
import com.task.hw_3.model.food.meat.Pork;

import java.util.ArrayList;


public class Zoo {
    public static void main(String[] args) throws WrongFoodException {
        ArrayList<Animal> animals = new ArrayList<>();
        animals.add(new Crocodile());
        animals.add(new Fish());
        animals.add(new Wolf());
        animals.add(new Cow());
        animals.add(new Duck());
        animals.add(new Horse());

        ArrayList<Food> foods = new ArrayList<>();
        foods.add(new Leaves());
        foods.add(new Roots());
        foods.add(new Vegetables());
        foods.add(new Beef());
        foods.add(new Mutton());
        foods.add(new Pork());


        Worker worker = new Worker();

        for (Animal a: animals) {
            System.out.println(a.toString());
            for (Food f: foods) {
                worker.feed(a, f);
            }
        }
        Voice v = (Voice) animals.get(0);
        worker.getVoice(v);
//        v = (Voice) animals.get(1); // Fish
//        worker.getVoice(v); doesn't work!!!

        ArrayList<Swim> pond = new ArrayList<>();
        pond.add(new Crocodile());
        pond.add(new Fish());
        pond.add(new Fish());
        pond.add(new Crocodile());
        pond.add(new Duck());

        for (Swim a: pond) {
            a.swim();
        }

        Aviary<Carnivorous> aviaryForCarnivorous = new Aviary(5);
        Crocodile crocodile = new Crocodile();
        crocodile.setName("ЗУБ");
        crocodile.setAviarySize(AviarySize.LARGE);
        if (!aviaryForCarnivorous.addAnimal(crocodile)) {
            System.out.println("Не подходит размер вольера");
        }
        System.out.println("KAVO " + aviaryForCarnivorous.getAnimal("ЗУБ"));
        aviaryForCarnivorous.delAnimal("ЗУБ");

        Aviary<Herbivore> aviaryForHerbivore = new Aviary(.5f);
        Duck duck = new Duck();
        duck.setName("Пингвин");
        duck.setAviarySize(AviarySize.SMALL);
        if (!aviaryForHerbivore.addAnimal(duck)) {
            System.out.println("Не подходит размер вольера");
        }
        System.out.println("KAVO " + aviaryForHerbivore.getAnimal("Пингвин"));
    }
}
