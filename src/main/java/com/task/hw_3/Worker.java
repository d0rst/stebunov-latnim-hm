package com.task.hw_3;

import com.task.hw_3.model.animals.Animal;
import com.task.hw_3.model.animals.impl.Voice;
import com.task.hw_3.model.food.Food;

public class Worker {
    public void feed(Animal animal, Food food){
        animal.eat(food);
    }
    public void getVoice(Voice animal){
            System.out.println(animal.voice());
    }
}
