package com.task.hw_8.steps;

import com.task.hw_8.ConfProperties;
import io.cucumber.java.ParameterType;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class WebDriverAvitoSteps {
    private WebDriver driver;
    private WebDriverWait webDriverWait;

    public WebDriverAvitoSteps() {
        System.setProperty("webdriver.chrome.driver", ConfProperties.getProperty("chromedriver"));
        this.driver = new ChromeDriver();
        this.webDriverWait = new WebDriverWait(driver, 2);
    }

    @ParameterType(".*")
    public CategoryAvito category(String category) {
        return CategoryAvito.valueOf(category);
    }

    @ParameterType(".*")
    public FilterSort filterSort(String filterSort){
        return FilterSort.valueOf(filterSort);
    }

    @Пусть("открыт ресурс авито")
    public void getUrl(){
        driver.get("https://www.avito.ru/");
        driver.manage().window().maximize();
    }

    @И("в выпадающем списке категорий выбрана {category}")
    public void selectCategory(CategoryAvito category){
        new Select(driver.findElement(By.xpath("//select[@name=\"category_id\"]")))
                .selectByVisibleText(category.getValue());
    }

    @И("в поле поиска введено значение {word}")
    public void inputSearch(String search){
        driver.findElement(By.xpath("//input[@data-marker=\"search-form/suggest\"]")).sendKeys(search);
    }

    @Тогда("кликнуть по выпадающему списку региона")
    public void selectMenuTown(){
        driver.findElement(By.xpath("//div[@class=\"main-text-2PaZG\"]")).click();
    }

    @Тогда("в поле регион введено значение {word}")
    public void searchTown(String town){
        driver.findElement(By.xpath("//div[@data-marker=\"popup-location/region\"]//input"))
                .sendKeys(town);
        webDriverWait.until(presenceOfElementLocated(
                By.xpath("//ul[@class=\"suggest-suggests-bMAdj\"]//li[@data-marker=\"suggest(0)\"]" +
                        "//*[contains(text(),'"+ town +"')]"))).click();;
    }

    @И("нажата кнопка показать объявления")
    public void searchClick(){
        driver.findElement(By.xpath("//button[@data-marker=\"popup-location/save-button\"]"))
                .click();
    }

    @Тогда("открылась страница результаты по запросу {word}")
    public void requestPage(String word) {
        String text = driver.findElement(By.cssSelector(".page-title-text-WxwN3")).getText();
        Assert.assertTrue(text.contains(word));
    }

    @И("активирован чекбокс только с фотографией")
    public void checkboxWithPhoto() {
        WebElement checkBox = driver.findElement(By.xpath("//input[contains(@data-marker,'with-images')]"));
        if (!checkBox.isSelected()) {
            checkBox.findElement(By.xpath("./parent::*")).click();
        }
    }

    public void clickCheckboxAndSearch(){
        WebElement checkbox = driver.findElement(
                By.xpath("//label[@data-marker=\"delivery-filter\"]"));
        if(checkbox.isEnabled()){
            checkbox.click();
            driver.findElement(By.xpath("//button[@data-marker=\"search-filters/submit-button\"]")).click();
        }
    }

    @И("в выпадающем списке сортировка выбрано значение <filterSort>")
    public void selectPriceFilter(FilterSort filterSort){
        new Select(driver.findElement(By.xpath("//div[@class=\"" +
                "sort-select-3QxXG select-select-box-3LBfK select-size-s-2gvAy\"]//select")))
                .selectByVisibleText(filterSort.getValue());
    }

    @И("в консоль выведено значение названия и цены {int} первых товаров")
    public void findObjectNameAndPrice(int n){
        List<WebElement> webElements = driver.findElements(
                By.xpath("//div[@data-marker=\"catalog-serp\"]/div[@data-marker=\"item\"]"));
        for (int i = 0; i < n; i++) {
            System.out.println(webElements.get(i).findElement(By.xpath(".//h3[@itemprop=\"name\"]"))
                    .getText());
            System.out.println(webElements.get(i).findElement(By.xpath(".//meta[@itemprop=\"price\"]"))
                    .getAttribute("content") + " рублей");
        }
    }

    @Attachment
    @Step("Make screen shot of results page")
    public byte[] makeScreenShot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    @Attachment
    @Step("Make screen shot of element on the page")
    public byte[] makeAshotScreenShot(WebElement element) {
        Screenshot screenshot = new AShot().shootingStrategy(
                ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver, element);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        try {
            ImageIO.write(screenshot.getImage(), "png", buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer.toByteArray();
    }

    public void quit() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }
}
