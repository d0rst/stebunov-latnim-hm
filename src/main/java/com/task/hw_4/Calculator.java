package com.task.hw_4;

import com.task.hw_4.services.CalculatorServes;

public class Calculator {
    public Float add(Object a, Object b) {
        Float[] f = convert(a, b);
        System.out.println("Складываем "+ a + " + " + b);
        return f[0] + f[1];
    }

    public Float mul(Object a, Object b) {
        Float[] f = convert(a, b);
        System.out.println("Умножаем "+ a + " * " + b);
        return roundOff(f[0] * f[1]);
    }

    public Float div(Object a, Object b) {
        Float[] f = convert(a, b);
        if (f[1]==0) {
            System.out.println("На ноль делить нельзя!");
            return null;
        }
        System.out.println("Делим "+ a + " / " + b);
        return roundOff(f[0] / f[1]);
    }

    public Float sub(Object a, Object b) {
        Float[] f = convert(a, b);
        System.out.println("Вычитаем "+ a + " - " + b);
        return f[0] - f[1];
    }

    private Float[] convert(Object a, Object b){
        if (!CalculatorServes.checkNum(a) || !CalculatorServes.checkNum(b)){
            System.out.println("Неверный ввод");
            return new Float[]{null, null};
        }
        return new Float[]{CalculatorServes.convector(a), CalculatorServes.convector(b)};
    }

    private float roundOff(float x) {
        float a = x;
        double temp = Math.pow(10.0, 2);
        a *= temp;
        a = Math.round(a);
        return (a / (float)temp);
    }
}
