package com.task.hw_4;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Calculator calculator = new Calculator();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Калькулятор!");
        boolean ex = true;
        while (ex){
            System.out.println("Введите два числа: ");
            String a = scanner.nextLine();
            String b = scanner.nextLine();

            System.out.println(
                    "Введите действие: \n" +
                            "1: Сложение  \n" +
                            "2: Умножение \n" +
                            "3: Деление   \n" +
                            "4: Вычитание \n" +
                            "0: Выход \n" );
            int x = scanner.nextInt();

            switch (x) {
                case 0 -> ex = false;
                case 1 -> System.out.println("Ответ: " + calculator.add(a, b));
                case 2 -> System.out.println("Ответ: " + calculator.mul(a, b));
                case 3 -> System.out.println("Ответ: " + calculator.div(a, b));
                case 4 -> System.out.println("Ответ: " + calculator.sub(a, b));
                default -> System.out.println("Действие не выбрано или введено некорректно!");
            }
        }
    }
}
