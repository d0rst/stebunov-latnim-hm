package com.task.hw_4.services;

import org.apache.commons.lang3.math.NumberUtils;

public class CalculatorServes {
    public static boolean checkNum(Object s){
        if (s instanceof String){
            String S = (String) s;
            S = S.replaceAll("\\s+","").replaceAll(",",".");
            return NumberUtils.isCreatable(S);
        }
        return s instanceof Number;
    }

    public static Float convector(Object o){
        if (o instanceof String){
            return Float.parseFloat((String) o);
        }
        if (o instanceof Integer){
            return ((Integer) o).floatValue();
        }
        if (o instanceof Double){
            return ((Double) o).floatValue();
        }
        //..и тд
        assert o instanceof Float;
        return (Float) o;
    }

}
