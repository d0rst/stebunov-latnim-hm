package com.task.hw_6;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", ConfProperties.getProperty("chromedriver"));
        WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, 2);

        driver.get("https://www.avito.ru/");
        driver.manage().window().maximize();

        Select category = new Select(driver.findElement(By.xpath("//select[@name=\"category_id\"]")));
        category.selectByVisibleText("Оргтехника и расходники");
        driver.findElement(By.xpath("//input[@id=\"search\"]")).sendKeys("Принтер");
        driver.findElement(By.xpath("//div[@class=\"main-text-2PaZG\"]")).click();
        driver.findElement(By.xpath("//div[@data-marker=\"popup-location/region\"]//input"))
                .sendKeys("Владивосток");
        wait.until(presenceOfElementLocated(
                By.xpath("//ul[@class=\"suggest-suggests-bMAdj\"]//li[@data-marker=\"suggest(0)\"]" +
                        "//*[contains(text(),'Владивосток')]"))).click();;

        driver.findElement(By.xpath("//button[@data-marker=\"popup-location/save-button\"]"))
                .click();



        WebElement checkbox = driver.findElement(
                By.xpath("//label[@data-marker=\"delivery-filter\"]"));
        if(checkbox.isEnabled()){
            checkbox.click();
            driver.findElement(By.xpath("//button[@data-marker=\"search-filters/submit-button\"]")).click();
        }

        new Select(driver.findElement(By.xpath("//div[@class=\"" +
                "sort-select-3QxXG select-select-box-3LBfK select-size-s-2gvAy\"]//select")))
                .selectByVisibleText("Дороже");

        List<WebElement> webElements = driver.findElements(
                By.xpath("//div[@data-marker=\"catalog-serp\"]/div[@data-marker=\"item\"]"));
        for (int i = 0; i < 3; i++) {
            System.out.println(webElements.get(i).findElement(By.xpath(".//h3[@itemprop=\"name\"]"))
                    .getText());
            System.out.println(webElements.get(i).findElement(By.xpath(".//meta[@itemprop=\"price\"]"))
                    .getAttribute("content") + " рублей");
        }

        driver.close();
    }
}
