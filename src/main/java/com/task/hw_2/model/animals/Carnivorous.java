package com.task.hw_2.model.animals;

import com.task.hw_2.model.food.Food;
import com.task.hw_2.model.food.Meat;

public abstract class Carnivorous extends Animal {

    @Override
    public void eat(Food food) {
        if (!(food instanceof Meat)){
            System.out.println("carnivorous don't eat it");
        } else {
            satietyValue += food.getSatietyValue();
            System.out.println("Animal start eat food: " + food.toString());
        }
    }

}
