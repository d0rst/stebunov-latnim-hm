package com.task.hw_2.model.animals.carnivorous;

import com.task.hw_2.model.animals.interfaces.Swim;
import com.task.hw_2.model.animals.Carnivorous;

public class Fish extends Carnivorous implements Swim {

    @Override
    public void swim() {
        System.out.println("fish swim");
    }

}
