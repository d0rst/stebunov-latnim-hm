package com.task.hw_2.model.animals.interfaces;

public interface Voice {
    String voice();
}
