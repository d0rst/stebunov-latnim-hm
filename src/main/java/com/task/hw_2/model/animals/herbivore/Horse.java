package com.task.hw_2.model.animals.herbivore;

import com.task.hw_2.model.animals.interfaces.Run;
import com.task.hw_2.model.animals.interfaces.Voice;
import com.task.hw_2.model.animals.Herbivore;

public class Horse extends Herbivore implements Run, Voice {

    @Override
    public void run() {
        this.satietyValue -= 5;
        System.out.println("Horse run");
    }

    @Override
    public String voice() {
        return "Horse neigh";
    }

}
