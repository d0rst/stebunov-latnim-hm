package com.task.hw_2.model.animals.herbivore;

import com.task.hw_2.model.animals.interfaces.Voice;
import com.task.hw_2.model.animals.Herbivore;

public class Cow extends Herbivore implements Voice {

    @Override
    public String voice() {
        return "Cow hums";
    }

}
