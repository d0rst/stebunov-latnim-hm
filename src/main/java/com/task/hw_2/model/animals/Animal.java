package com.task.hw_2.model.animals;

import com.task.hw_2.model.food.Food;

public abstract class Animal {

    protected int satietyValue;
    public abstract void eat(Food food);

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
