package com.task.hw_2.model.animals.herbivore;

import com.task.hw_2.model.animals.Herbivore;
import com.task.hw_2.model.animals.interfaces.Fly;
import com.task.hw_2.model.animals.interfaces.Run;
import com.task.hw_2.model.animals.interfaces.Swim;
import com.task.hw_2.model.animals.interfaces.Voice;

public class Duck extends Herbivore implements Fly, Swim, Run, Voice {

    @Override
    public void fly() {
        this.satietyValue -= 5;
        System.out.println("Duck fly");
    }

    @Override
    public void run() {
        this.satietyValue -= 5;
        System.out.println("Duck run");
    }

    @Override
    public void swim() {
        this.satietyValue -= 5;
        System.out.println("Duck swim");
    }

    @Override
    public String voice() {
        return "Duck quacks";
    }

}
