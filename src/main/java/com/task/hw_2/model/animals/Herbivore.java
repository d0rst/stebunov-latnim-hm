package com.task.hw_2.model.animals;

import com.task.hw_2.model.food.Food;
import com.task.hw_2.model.food.Grass;

public abstract class Herbivore extends Animal {

    @Override
    public void eat(Food food) {
        if (!(food instanceof Grass)){
            System.out.println("herbivores don't eat it");
        } else {
            satietyValue += food.getSatietyValue();
            System.out.println("Animal start eat food: " + food.toString());
        }
    }

}
