package com.task.hw_2.model.food;

import java.util.Date;

public abstract class Food {
    protected int satietyValue;
    protected Date shelfLife;

    public abstract void cookFood();

    public void setShelfLife(Date date){
        this.shelfLife = date;
    }

    public int getSatietyValue() {
        return satietyValue;
    }
}
