package com.task.hw_2.model.food.meat;

import com.task.hw_2.model.food.Meat;

import java.util.Date;

public class Beef extends Meat {

    public Beef() {
        this.shelfLife = new Date();
        this.satietyValue = 8;
    }

    @Override
    public String toString() {
        return "Beef";
    }

}
