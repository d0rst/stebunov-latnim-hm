package com.task.hw_2.model.food.grass;

import com.task.hw_2.model.food.Grass;

import java.util.Date;

public class Roots extends Grass {

    public Roots() {
        this.shelfLife = new Date();
        this.satietyValue = 3;
    }

    @Override
    public String toString() {
        return "Roots";
    }

}
