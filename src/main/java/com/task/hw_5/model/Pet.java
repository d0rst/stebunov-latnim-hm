package com.task.hw_5.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Pet {

    @SerializedName("photoUrls")
    private List<String> photoUrls;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;

    @SerializedName("category")
    private Category category;

    @SerializedName("tags")
    private List<TagsItem> tags;

    @SerializedName("status")
    private String status;

    public void setPhotoUrls(List<String> photoUrls) {
        this.photoUrls = photoUrls;
    }

    public List<String> getPhotoUrls() {
        return photoUrls;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }
}