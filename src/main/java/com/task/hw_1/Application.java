package com.task.hw_1;

import com.task.hw_1.model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik firstKotik = new Kotik(4, "кот1",5,6, "мяу");
        Kotik secondKotik = new Kotik();
        secondKotik.setAge(3);
        secondKotik.setName("кот2");
        secondKotik.setWeight(4);
        secondKotik.setSatietyValue(5);
        secondKotik.setMeow("мяу");

        firstKotik.liveAnotherDay();
        System.out.println("имя " + firstKotik.getName() + " вес " + firstKotik.getWeight());

        System.out.println(firstKotik.getMeow().equals(secondKotik.getMeow()) ? "одинаково" : "неодинаково");

        System.out.println("количество котиков: " + Kotik.getCatCounter());
    }
}
