package com.task.hw_1.model;

import java.util.Scanner;

public class Kotik {
    private static int catCounter = 0;

    private String meow;
    private int satietyValue;
    private String name;
    private float weight;
    private int age;

    public Kotik(int satietyValue, String name, float weight, int age, String meow) {
        this.satietyValue = satietyValue;
        this.name = name;
        this.weight = weight;
        this.age = age;
        this.meow = meow;
        catCounter++;
    }

    public Kotik() {
        catCounter++;
    }

    public void liveAnotherDay(){
        for (int i = 0; i < 24; i++) {
//            System.out.println("Дейстиве номер: " + i);
//            System.out.println("satietyValue: " + satietyValue);
            int actionId = (int) (Math.random() * 5) + 1;
//            switch (actionId) {
//                case (1) -> action(play());
//                case (2) -> action(sleep());
//                case (3) -> action(chaseMouse());
//                case (4) -> action(run());
//                case (5) -> action(sayMeow());
//            }
        }
    }

    private void action(boolean a){
        if (!a) {
            Scanner sc = new Scanner(System.in);
//            System.out.println("the cat does nothing");
            System.out.println("i want to eat!!!, feed the cat? [y/n]");
            String s = sc.next();
            if ("y".equals(s)){
                eat();
            }
        }
    }

    private boolean play(){
        if (satietyValue <= 0) {
            return false;
        }
        System.out.println("play");
        satietyValue -= 2;
        return true;
    }

    private boolean sleep(){
        if (satietyValue <= 0) {
            return false;
        }
        System.out.println("sleep");
        satietyValue -= 1;
        return true;
    }

    private boolean chaseMouse(){
        if (satietyValue <= 0) {
            return false;
        }
        System.out.println("chaseMouse");
        satietyValue -= 3;
        return true;
    }

    private boolean run(){
        if (satietyValue <= 0) {
            return false;
        }
        System.out.println("run");
        satietyValue -= 1;
        return true;
    }

    private boolean sayMeow(){
        if (satietyValue <= 0) {
            return false;
        }
        System.out.println("sayMeow");
        satietyValue -= 1;
        return true;
    }

    private void eat(int s){
        System.out.println("eat");
        satietyValue += s;
    }

    private void eat(int s, String foodName){
        System.out.println("eat");
        satietyValue += s;
    }

    private void eat(){
        eat(10, "cat food");
    }

    public int getSatietyValue() {
        return satietyValue;
    }

    public String getName() {
        return name;
    }

    public float getWeight() {
        return weight;
    }

    public int getAge() {
        return age;
    }

    public void setSatietyValue(int satietyValue) {
        this.satietyValue = satietyValue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMeow() {
        return meow;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    public static int getCatCounter() {
        return catCounter;
    }
}
