package com.task.hw_4;

import com.task.hw_4.services.CalculatorServes;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculatorTest {

    Calculator calculator = new Calculator();

    @DataProvider
    public Object[][] testMulVar() {
        return new Object[][]{
                {1, 1, 1f}, {2d, 2d, 4f},
                {"2", "2", 4f}, {"-2", "2", -4f},
                {3.5f, 2.5f, 8.75f}, {2.3d, 2.6d, 5.98f},
                {"qwe", "qwe", null}, {"qwe", "2.5", null},
                {"2.5", "qwe", null}, {2.3, "qwe", null},
                {5, 0, 0f}, {0, -2, 0f},
                {-5, 0, 0f}, {2, 0, 0f},
                {-0.00005, 2, null}, {0.00005, -2, null},
                {-1110.00005, 2, null}, {11110.00005, 2, null},

        };
    }

    @DataProvider
    public Object[][] testAddVar() {
        return new Object[][]{
                {1, 1, 2f}, {2d, 2d, 4f},
                {"2", "2", 4f}, {"-2", "2", 0f},
                {3.5f, 2.5f, 6f}, {2.3d, 2.6d, 4.9f},
                {"qwe", "qwe", null}, {"qwe", "2.5", null},
                {"2.5", "qwe", null}, {2.3, "qwe", null},
                {5, 0, 5f}, {0, -2, -2f},
                {-5, 0, -5f}, {2, 0, 2f},
                {-0.00005, 2, 1.99995}, {0.00005, -2, -1.99995},
                {-1110.00005, 2, -1108f}, {11110.00005, 2, 11112f},

        };
    }

    @DataProvider
    public Object[][] testDivVar() {
        return new Object[][]{
                {1, 1, 1f}, {2d, 2d, 1f},
                {"2", "2", 1f}, {"-2", "2", -1f},
                {3.5f, 2.5f, 1.4f}, {2.3d, 2.6d, 0.88f},
                {"qwe", "qwe", null}, {"qwe", "2.5", null},
                {"2.5", "qwe", null}, {2.3, "qwe", null},
                {5, 0, null}, {0, -2, 0f},
                {"-5", 0, null}, {"2", 0, null},
                {-0.00005, 2, null}, {0.00005, -2, null},
                {-1110.00005, 2, -555f}, {11110.00005, 2, 5555f},

        };
    }

    @DataProvider
    public Object[][] testSubVar() {
        return new Object[][]{
                {1, 1, 0f}, {2d, 4d, -2f},
                {"2", "2", 0f}, {"-2", "2", -4f},
                {3.5f, 2.5f, 1f}, {2.3d, 2.6d, -0.3f},
                {"qwe", "qwe", null}, {"qwe", "2.5", null},
                {"2.5", "qwe", null}, {2.3, "qwe", null},
                {5, 0, 5f}, {0, -2, 2f},
                {-5, 0, -5f}, {2, 0, 2f},
                {-0.00005, 2, null}, {0.00005, -2, null},
                {-1110.00005, 2, null}, {11110.00005, 2, null},

        };
    }

    @DataProvider
    public Object[][] testVarInput() {
        return new Object[][]{
                {1, true}, {2, true}, {'1', false}, {"2", true},
                {-1, true}, {2, true}, {"-2", true}, {"abc", false},
                {"123qwe", false}, {"qwe123", false}, {"-2.5", true}, {"2.5f", true},
                {"-123qwe", false}, {"-qwe123", false}, {"1,5", true}, {"2,5f", true}
        };
    }

    @Test(dataProvider = "testVarInput")
    public void testInput(Object a, Object ans) {
        Assert.assertEquals(CalculatorServes.checkNum(a), ans, "Неверная проверка!");
    }

    @Test(dataProvider = "testMulVar")
    public void testMul(Object a, Object b, Object ans) {
        Assert.assertEquals(calculator.mul(a, b), ans, "Неверное умножение!");
    }

    @Test(dataProvider = "testAddVar")
    public void testAdd(Object a, Object b, Object ans) {
        Assert.assertEquals(calculator.add(a, b), ans, "Неверное сложение!");
    }

    @Test(dataProvider = "testDivVar")
    public void testDiv(Object a, Object b, Object ans) {
        Assert.assertEquals(calculator.div(a, b), ans, "Неверное деление!");
    }

    @Test(dataProvider = "testSubVar")
    public void testSub(Object a, Object b, Object ans) {
        Assert.assertEquals(calculator.sub(a, b), ans, "Неверное вычитание!");
    }



}
