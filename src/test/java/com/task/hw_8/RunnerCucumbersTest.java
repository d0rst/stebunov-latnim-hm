package com.task.hw_8;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"pretty",
                "html:target/cucumber-reports/cucumber.html",
                "json:target/cucumber-reports/cucumber.json"
        },

        features = "src/test/java/features",
        glue = "com.task.hw_8.steps",
        tags = "@searchOnAvito_2"
)

public class RunnerCucumbersTest extends AbstractTestNGCucumberTests {
}
