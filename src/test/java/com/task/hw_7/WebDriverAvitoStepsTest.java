package com.task.hw_7;

import com.task.hw_8.steps.CategoryAvito;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class WebDriverAvitoStepsTest {

    private WebDriverAvitoSteps steps;
    private WebDriver driver;

    private final CategoryAvito category = CategoryAvito.оргтехника;
    private final String search = "принтер";
    private final String town = "Владивосток";
    private final String sort = "Дороже";

    @DataProvider
    public static Object[][] provider() {
        return new Object[][]{
                {"Оргтехника и расходники", "принтер", "Владивосток", "Дороже", 3},
                {"Оргтехника и расходники", "принтер", "Тольятти", "Дешевле", 5}
        };
    }

    @BeforeMethod
    public void setUp() {
        steps = new WebDriverAvitoSteps();
        driver = steps.getDriver();
    }

    @Test(description = "Выбор категории")
    public void categoryTest() {
        steps.getUrl();
        steps.makeScreenShot();
        steps.selectCategory(this.category);
        steps.makeAshotScreenShot(driver.findElement(By.xpath("//div[@data-marker=\"search-form\"]")));
    }

    @Test(description = "Поиск")
    public void searchTest() {
        steps.getUrl();
        steps.selectCategory(category);
        steps.makeScreenShot();
        steps.inputSearch(this.search);
        steps.makeAshotScreenShot(driver.findElement(By.xpath("//div[@data-marker=\"search-form\"]")));
    }

    @Test(description = "Ввод и выбор города")
    public void inputAndSearchTownTest() {
        steps.getUrl();
        steps.selectCategory(this.category);
        steps.inputSearch(this.search);
        steps.makeScreenShot();
        steps.selectMenuTown();
        steps.makeAshotScreenShot(driver.findElement(By.xpath("//div[@data-marker=\"popup-location/content\"]")));
    }

    @Test(description = "Выбор города")
    public void searchTownTest() {
        steps.getUrl();
        steps.selectCategory(this.category);
        steps.inputSearch(this.search);
        steps.selectMenuTown();
        steps.makeScreenShot();
        steps.searchTownAndClick(this.town);
        steps.makeAshotScreenShot(driver.findElement(By.xpath("//div[@data-marker=\"popup-location/content\"]")));
    }

    @Test( description = "Проверка чекбокса")
    public void checkboxTest() {
        steps.getUrl();
        steps.selectCategory(this.category);
        steps.inputSearch(this.search);
        steps.selectMenuTown();
        steps.searchTownAndClick(this.town);
        steps.makeScreenShot();
        steps.clickCheckboxAndSearch();
        steps.makeAshotScreenShot(driver.findElement(
                By.xpath("//div[@data-marker=\"fieldset/d\"]")));
    }

    @Test(description = "Проверка сортировки по цене")
    public void chekPriceFilterTest() {
        steps.getUrl();
        steps.selectCategory(this.category);
        steps.inputSearch(this.search);
        steps.selectMenuTown();
        steps.searchTownAndClick(this.town);
        steps.clickCheckboxAndSearch();
        steps.makeScreenShot();
        steps.selectPriceFilter(this.sort);
        steps.makeAshotScreenShot(driver.findElement(By.xpath("//div[@class=\"index-topPanel-1F0TP\"]")));
    }

    @Test(description = "Вывод наименования товара и цены")
    public void nameAndPriceTest(CategoryAvito category, String search, String town, String sort, int n) {
        steps.getUrl();
        steps.selectCategory(category);
        steps.inputSearch(search);
        steps.selectMenuTown();
        steps.searchTownAndClick(town);
        steps.clickCheckboxAndSearch();
        steps.selectPriceFilter(sort);
        steps.makeScreenShot();
        steps.findObjectNameAndPrice(n);
        steps.makeAshotScreenShot(driver.findElement(
                By.xpath("//div[@data-marker=\"catalog-serp\"]")));
    }

    @AfterMethod
    public void tearDown() {
        steps.quit();
    }

}
