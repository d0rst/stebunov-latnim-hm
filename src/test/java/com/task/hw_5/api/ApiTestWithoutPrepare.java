package com.task.hw_5.api;

import io.restassured.http.ContentType;
import io.restassured.http.Header;
import org.testng.annotations.Test;

import java.io.IOException;

import static io.restassured.RestAssured.given;


public class ApiTestWithoutPrepare {
    @Test
    public void testGet() throws IOException {
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("my.my.properties"));
        given().baseUri("https://petstore.swagger.io/v2/")
                .header(new Header("api_key", System.getProperty("api.key")))
                .accept(ContentType.JSON)
                .pathParam("petId", System.getProperty("petId"))
                .log().all()
                .when()
                .get("/pet/{petId}")
                .then()
                .statusCode(200)
                .log().all();
    }
}