package com.task.hw_5.api;

import com.google.gson.annotations.SerializedName;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import com.task.hw_5.store.Order;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class HomeTaskApiTest {

    public static final int ORDER_ID = 7;

    @BeforeClass
    public void prepare() throws IOException {
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("my.properties"));
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://petstore.swagger.io/v2/")
                .addHeader("api_key", System.getProperty("api.key"))
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();

        RestAssured.filters(new ResponseLoggingFilter());
    }

    @Test
    public void checkOrderCreate(){
        Order order = new Order();
        order.setId(ORDER_ID);
        order.setPetId(0);
        order.setQuantity(0);
        order.setShipDate("2021-05-21T21:31:40.221Z");
        order.setStatus("placed");
        order.setComplete(true);

        given().body(order)
                .when()
                .post("/store/order")
                .then()
                .statusCode(200);

        Order actual = given().pathParam("orderId", ORDER_ID)
                .when()
                .get("/store/order/{orderId}")
                .then()
                .statusCode(200)
                .extract().body()
                .as(Order.class);

        Assert.assertEquals(actual.getId(), order.getId());
        Assert.assertEquals(actual.getPetId(), order.getPetId());
        Assert.assertEquals(actual.getQuantity(), order.getQuantity());
        Assert.assertEquals(actual.getShipDate(), order.getShipDate());
        Assert.assertEquals(actual.getStatus(), order.getStatus());
    }

    @Test
    public void testOrderDelete(){
        Order order = new Order();
        order.setId(ORDER_ID);
        order.setPetId(0);
        order.setQuantity(0);
        order.setShipDate("2021-05-21T21:31:40.221Z");
        order.setStatus("placed");
        order.setComplete(true);

        given().body(order)
                .when()
                .post("/store/order")
                .then()
                .statusCode(200);

        given()
                .pathParam("orderId", ORDER_ID)
                .when()
                .delete("/store/order/{orderId}")
                .then()
                .statusCode(200);
        given()
                .pathParam("orderId", ORDER_ID)
                .when()
                .get("/store/order/{orderId}")
                .then()
                .statusCode(404);
    }

    @Test
    public void testStoreInventory(){
        Map inventory = given()
                .when()
                .get("/store/inventory")
                .then()
                .statusCode(200)
                .extract().body()
                .as(Map.class);
        Assert.assertFalse(inventory.containsKey("XXX"), "Inventory не содержит XXX" );
    }
}